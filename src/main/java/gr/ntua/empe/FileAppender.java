package gr.ntua.empe;

import gr.ntua.empe.prometheus.gps.GpsEvent;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Stupid way to write to a file...
 */
public class FileAppender {

    SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yy_HH_mm_ss");

    PrintWriter writer = null;

    public FileAppender() throws FileNotFoundException, UnsupportedEncodingException {
        String filename = "gps_data_" + sdf.format(new Date()) + ".csv";
        System.out.println("Writing to file: " + filename);
        writer = new PrintWriter(filename, "UTF-8");
        writer.println("timestamp,latitude,longitude,altitude,current_speed,expected_speed,zone");
        writer.flush();
    }

    public void newPosition(GpsEvent event, DrivingZone zone) {
        writer.println(event.getTimeStamp() + "," +
                event.getLatitude() + "," +
                event.getLongitude() + "," +
                event.getAltitude() + "," +
                event.getSpeed() + "," +
                zone.getSpeed() + "," +
                zone.getName());
        writer.flush();
    }
}