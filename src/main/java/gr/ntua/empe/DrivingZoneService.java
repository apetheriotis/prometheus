package gr.ntua.empe;


import ch.hsr.geohash.WGS84Point;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * http://boundingbox.klokantech.com/ and select CSV on the bottom right
 */
public class DrivingZoneService {

    DrivingZone defaultZone = new DrivingZone("N/A",0.0, 0.0, 0, 0, 0, 0);
    private List<DrivingZone> drivingZones = new ArrayList<>();

    public DrivingZoneService(String filename) throws IOException {
        System.out.print("Setting zone for file:" + filename);

        FileInputStream inputStream;
        ClassLoader classLoader = this.getClass().getClassLoader();
        File configFile=new File(classLoader.getResource(filename).getFile());

        inputStream = new FileInputStream(configFile);
        int lineNo = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = reader.readLine()) != null) {
            lineNo++;
            if (lineNo == 1) continue;

            String[] values = line.split(",");
            DrivingZone zone = new DrivingZone(
                    values[0],
                    Double.parseDouble(values[1]),
                    Double.parseDouble(values[2]),
                    new WGS84Point(Double.parseDouble(values[3]), Double.parseDouble(values[4])),
                    new WGS84Point(Double.parseDouble(values[5]), Double.parseDouble(values[6])));
            drivingZones.add(zone);
        }

    }


    public DrivingZone getDrivingZone(Double lat, Double lon) {
        DrivingZone drivingZone = null;
        for (DrivingZone zone : drivingZones) {
            if (zone.contains(new WGS84Point(lat, lon))) {
                drivingZone = zone;
            }
            System.out.println(zone.contains(new WGS84Point(lat, lon)) + "  center " + zone.getCenterPoint());
        }

        if (drivingZone == null) {
            return defaultZone;
        } else {
            return drivingZone;
        }
    }

}