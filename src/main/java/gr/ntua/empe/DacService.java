package gr.ntua.empe;


import com.pi4j.gpio.extension.mcp.MCP4725GpioProvider;
import com.pi4j.gpio.extension.mcp.MCP4725Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogOutput;
import com.pi4j.io.i2c.I2CBus;

import java.io.IOException;

public class DacService {

    GpioPinAnalogOutput vout = null;

    public DacService() throws IOException {
        final GpioController gpio = GpioFactory.getInstance();
        final MCP4725GpioProvider gpioProvider = new MCP4725GpioProvider(I2CBus.BUS_1, MCP4725GpioProvider.MCP4725_ADDRESS_1);
        vout = gpio.provisionAnalogOutputPin(gpioProvider, MCP4725Pin.OUTPUT);
    }

    public void setVout(double v) {
        vout.setValue(v);
    }
}
