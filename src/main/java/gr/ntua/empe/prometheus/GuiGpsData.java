package gr.ntua.empe.prometheus;


public class GuiGpsData {

    private String datetime;
    private double latitude;
    private double longitude;
    private double altitude;
    private double actualSpeed;
    private double expectedSpeed;
    private String drivingZone;
    private String drivingMode;

    public GuiGpsData(String datetime, double latitude, double longitude, double altitude, double actualSpeed,
                      double expectedSpeed, String drivingZone, String drivingMode) {
        this.datetime = datetime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.actualSpeed = actualSpeed;
        this.expectedSpeed = expectedSpeed;
        this.drivingZone = drivingZone;
        this.drivingMode = drivingMode;
    }


    public String getDrivingMode() {
        return drivingMode;
    }

    public String getDrivingZone() {
        return drivingZone;
    }

    public String getDatetime() {
        return datetime;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public double getActualSpeed() {
        return actualSpeed;
    }

    public double getExpectedSpeed() {
        return expectedSpeed;
    }
}
