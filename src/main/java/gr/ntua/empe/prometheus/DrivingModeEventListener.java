package gr.ntua.empe.prometheus;

public interface DrivingModeEventListener {
    void drivingModeUpdated(String drivingMode);
}