package gr.ntua.empe.prometheus;

import gr.ntua.empe.prometheus.gps.GpsEvent;

public interface GpsGuiListener {
    void newData(GuiGpsData gpsData);
}