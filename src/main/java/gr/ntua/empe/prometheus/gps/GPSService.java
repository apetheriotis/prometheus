package gr.ntua.empe.prometheus.gps;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GPSService {

    private static final String SERIAL_PORT = "/dev/ttyUSB0";

    private List<GpsEventListener> gpsEventListeners = new ArrayList<>();

    public void addListener(GpsEventListener listener) {
        gpsEventListeners.add(listener);
    }

    public void startApp() throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        try (GPSCommSensor gps = new GPSCommSensor(SERIAL_PORT)) {

            Position lastPosition = null;

            /* Take one reading every second for 10 readings */
            while (true) {
                Position p = gps.getPosition();
                Double speed = getSpeed(lastPosition, p);
                System.out.println("----------------");
                System.out.println(p.getLatitudeStr() +"  " + p.getLongitudeStr());
                System.out.println(p.getLatitude() +"  " + p.getLatitude());
                for (GpsEventListener listener : gpsEventListeners) listener.newPosition(new GpsEvent(p, speed));
                lastPosition = p;
                Thread.sleep(1000);
            }
        } catch (IOException ioe) {
            System.out.println("GPSTestMidlet: IOException " + ioe.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("GPSTestMidlet finished");

    }

    private Double getSpeed(Position pos1, Position pos2) {
        if (pos1 == null || pos2 == null) return 99.99;
        Double distance = getDistanceOnGeoid(
                pos1.getLatitudeNormalized(), pos1.getLongitudeNormalized(),
                pos2.getLatitudeNormalized(), pos2.getLongitudeNormalized());
        long timeDuration = (pos2.getTimeStamp() - pos1.getTimeStamp());
        double speedMps = (distance / timeDuration)/1000;
        double speedKph = (speedMps * 3600.0) / 1000.0;
        return (double) Math.round(speedKph * 10d) / 10d;
    }

    private Double getDistanceOnGeoid(double lat1, double lon1, double lat2, double lon2) {

        // Convert degrees to radians
        lat1 = lat1 * Math.PI / 180.0;
        lon1 = lon1 * Math.PI / 180.0;

        lat2 = lat2 * Math.PI / 180.0;
        lon2 = lon2 * Math.PI / 180.0;

        // radius of earth in metres
        double r = 6378100;

        // P
        Double rho1 = r * Math.cos(lat1);
        double z1 = r * Math.sin(lat1);
        double x1 = rho1 * Math.cos(lon1);
        double y1 = rho1 * Math.sin(lon1);

        // Q
        double rho2 = r * Math.cos(lat2);
        double z2 = r * Math.sin(lat2);
        double x2 = rho2 * Math.cos(lon2);
        double y2 = rho2 * Math.sin(lon2);

        // Dot product
        double dot = (x1 * x2 + y1 * y2 + z1 * z2);
        double cos_theta = dot / (r * r);

        double theta = Math.acos(cos_theta);

        // Distance in Metres
        return r * theta;
    }


}
