package gr.ntua.empe.prometheus.gps;

public interface GpsEventListener {
    void newPosition(GpsEvent event);
}