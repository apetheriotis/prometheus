
package gr.ntua.empe.prometheus.gps;

import gnu.io.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Adafruit Ultimate GPS sensor connected to the Raspberry Pi via a serial port
 */
public class GPSCommSensor extends GPSSensor {

    public GPSCommSensor(String serialPortId) throws IOException, NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(serialPortId);
        SerialPort serialPort = (SerialPort) portIdentifier.open("GPS_Connection", 0);
        serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        InputStream serialInputStream = serialPort.getInputStream();
        serialBufferedReader = new BufferedReader(new InputStreamReader(serialInputStream));
        System.out.println("AdaFruit GPS Sensor: READY");
    }


    @Override
    public void close() throws IOException {
        serialBufferedReader.close();
    }
}
