package gr.ntua.empe.prometheus.gps;

public class GpsEvent {

    private long timeStamp;
    private double latitude;
    private double longitude;
    private double altitude;
    private double speed;

    public GpsEvent(Position position, double speed) {
        timeStamp = position.getTime();
        latitude = position.getLatitudeNormalized();
        longitude = position.getLongitudeNormalized();
        altitude = position.getAltitude();
        this.speed = speed;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public double getSpeed() {
        return speed;
    }

}