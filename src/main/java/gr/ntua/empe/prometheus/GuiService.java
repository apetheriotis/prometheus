package gr.ntua.empe.prometheus;


import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class GuiService extends JFrame implements GpsGuiListener {

    java.util.List<String> availableDrivingModes = new ArrayList<>();
    Integer currentModeIndex = 0;

    JButton countdownStart = new JButton("Start/reset");
    JLabel timestampText = new JLabel("00:00");
    JLabel zoneTitle = new JLabel("You are driving in zone: ");
    JLabel zoneText = new JLabel("AB");
    JLabel coordsTitle = new JLabel("Lat,Lon: ");
    JLabel coordsText = new JLabel("37.99422,27.99422");
    JLabel currentSpeedText = new JLabel("25.5");
    JLabel expectedSpeedText = new JLabel("24.8");
    JLabel currentSpeedSymbol = new JLabel("km/h");
    JLabel expectedSpeedSymbol = new JLabel("km/h");
    JButton modeUp = new JButton("Up");
    JLabel drivingModeText = new JLabel("N/A");
    JButton modeDown = new JButton("Down");


    // time stuff
    DateTime dt2 = new DateTime().plusMinutes(42);

    Timer timer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int noOfMinutes = Minutes.minutesBetween(new DateTime(), dt2).getMinutes();
            int noSec = Seconds.secondsBetween(new DateTime(), dt2).getSeconds() - noOfMinutes * 60;
            timestampText.setText(noOfMinutes + ":" + noSec);
        }
    });


    private java.util.List<DrivingModeEventListener> listeners = new ArrayList<>();

    public void addListener(DrivingModeEventListener listener) {
        listeners.add(listener);
    }


    public GuiService() {
        // Add mode names here. But you need to change the "backend" code to include the corresponding csv
        availableDrivingModes.add("mode1");
        availableDrivingModes.add("mode2");
        availableDrivingModes.add("mode3");
        drivingModeText.setText(availableDrivingModes.get(currentModeIndex));

        countdownStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Reset timer?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) { //The ISSUE is here
                    dt2 = new DateTime().plusMinutes(42);
                    timer.start();
                }
            }
        });

        modeUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (currentModeIndex < availableDrivingModes.size() - 1) {
                    currentModeIndex++;
                    drivingModeText.setText(availableDrivingModes.get(currentModeIndex));
                    for (DrivingModeEventListener listener : listeners)
                        listener.drivingModeUpdated(availableDrivingModes.get(currentModeIndex));
                }
            }
        });

        modeDown.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (currentModeIndex >= 1) {
                    currentModeIndex--;
                    drivingModeText.setText(availableDrivingModes.get(currentModeIndex));
                    for (DrivingModeEventListener listener : listeners)
                        listener.drivingModeUpdated(availableDrivingModes.get(currentModeIndex));
                }
            }
        });
    }


    public void initUI() {
        setTitle("Prometheus Core");
        setSize(320, 240);
        setDefaultCloseOperation(EXIT_ON_CLOSE);


        //////// -------------- DATA PANELS -------------- ////////
        timestampText.setFont(new Font("Courier New", Font.BOLD, 15));
        coordsText.setFont(new Font("Courier New", Font.BOLD, 15));
        zoneText.setFont(new Font("Courier New", Font.BOLD, 15));

        // Coordinates panel
        JPanel coordsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        coordsPanel.add(coordsTitle);
        coordsPanel.add(coordsText);

        // Mode selection panel
        JPanel modePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        modePanel.add(modeUp);
        modePanel.add(drivingModeText);
        modePanel.add(modeDown);

        // Timestamp panel
        JPanel timePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        timePanel.add(countdownStart);
        timePanel.add(timestampText);

        // Zone panel
        JPanel zonePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        zonePanel.add(zoneTitle);
        zonePanel.add(zoneText);

        JPanel dataPanel = new JPanel(new GridLayout(3, 1, 0, 0));
        dataPanel.setBorder(BorderFactory.createTitledBorder("Data"));
        dataPanel.setPreferredSize(new Dimension(310, 100));
        dataPanel.add(timePanel);
//        dataPanel.add(coordsPanel);
        dataPanel.add(modePanel);
        dataPanel.add(zonePanel);


        //////// -------------- SPEEDS PANELS -------------- ////////

        // Current speed/ Expected speed text setup
        currentSpeedText.setFont(new Font("Courier New", Font.BOLD, 36));
        expectedSpeedText.setFont(new Font("Courier New", Font.BOLD, 36));

        // Current speed panel
        JPanel currentSpeedPanel = new JPanel();
        currentSpeedPanel.setBorder(BorderFactory.createTitledBorder("Current"));
        currentSpeedPanel.add(currentSpeedText);
        currentSpeedPanel.add(currentSpeedSymbol);

        // Expected speed panel
        JPanel expectedSpeedPanel = new JPanel();
        expectedSpeedPanel.setBorder(BorderFactory.createTitledBorder("Expected"));
        expectedSpeedPanel.add(expectedSpeedText);
        expectedSpeedPanel.add(expectedSpeedSymbol);

        JPanel speedsPanel = new JPanel();
        speedsPanel.setPreferredSize(new Dimension(310, 100));
        speedsPanel.add(currentSpeedPanel);
        speedsPanel.add(expectedSpeedPanel);

        // Add all to main panel
        setLayout(new GridLayout(2, 1, 0, 0));
        add(dataPanel);
        add(speedsPanel);
        pack();
        setVisible(true);
    }


    @Override
    public void newData(GuiGpsData gpsData) {
        //timestampText.setText(gpsData.getDatetime());
        coordsText.setText(new DecimalFormat("##.#####").format(gpsData.getLatitude()) +
                "," + new DecimalFormat("##.#####").format(gpsData.getLongitude()));
        currentSpeedText.setText(new DecimalFormat("##.##").format(gpsData.getActualSpeed()));
        expectedSpeedText.setText(new DecimalFormat("##.##").format(gpsData.getExpectedSpeed()));
        zoneText.setText(gpsData.getDrivingZone());
    }
}