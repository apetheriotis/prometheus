package gr.ntua.empe;

import gr.ntua.empe.prometheus.GuiService;
import gr.ntua.empe.prometheus.gps.GPSService;

/**
 * A Camel Application
 */
public class MainApp {


    public static void main(String... args) throws Exception {

        // Start UI
        GuiService guiService = new GuiService();
        guiService.initUI();

        // Start coordinator
        Coordinator coordinator = new Coordinator();

        guiService.addListener(coordinator);
        coordinator.addListener(guiService);

        // Start GPS sevice
        GPSService gpsService = new GPSService();
        gpsService.addListener(coordinator);
        gpsService.startApp();


        System.out.println("Stared/finished? app ------------- ");

    }

}

