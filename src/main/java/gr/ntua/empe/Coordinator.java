package gr.ntua.empe;

import gr.ntua.empe.prometheus.DrivingModeEventListener;
import gr.ntua.empe.prometheus.GpsGuiListener;
import gr.ntua.empe.prometheus.GuiGpsData;
import gr.ntua.empe.prometheus.gps.GpsEvent;
import gr.ntua.empe.prometheus.gps.GpsEventListener;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Coordinator implements GpsEventListener, DrivingModeEventListener {

    // Start file appender
    FileAppender fileAppender = null;
    private Map<String, DrivingZoneService> drivingZones = new HashMap<>();

    private DacService dacService = new DacService();

    public Coordinator() throws IOException {
        fileAppender = new FileAppender();
        drivingZones.put("mode1", new DrivingZoneService("mode1.csv"));
        drivingZones.put("mode2", new DrivingZoneService("mode2.csv"));
        drivingZones.put("mode3", new DrivingZoneService("mode3.csv"));
    }

    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS (dd-MM-yy)");
    private List<GpsGuiListener> listeners = new ArrayList<>();

    public void addListener(GpsGuiListener listener) {
        listeners.add(listener);
    }

    private String currentDrivingZone = "N/A";

    @Override
    public void newPosition(GpsEvent event) {
        Date date = new Date(event.getTimeStamp());
        System.out.println("Received GPS at " + sdf.format(date) + "   " + event.getLatitude() + "   " + event.getLongitude());

        DrivingZone zone = drivingZones.get(currentDrivingZone).getDrivingZone(event.getLatitude(), event.getLongitude());
        dacService.setVout(zone.getVout());

        fileAppender.newPosition(event, zone);
        GuiGpsData gps = new GuiGpsData(
                sdf.format(date),
                event.getLatitude(),
                event.getLongitude(),
                event.getAltitude(),
                event.getSpeed(),
                zone.getSpeed(),
                zone.getName(),
                currentDrivingZone);
        for (GpsGuiListener listener : listeners) listener.newData(gps);

    }

    @Override
    public void drivingModeUpdated(String drivingMode) {
        System.out.println("Setting driving mode to : " + drivingMode);
        currentDrivingZone = drivingMode;
    }
}