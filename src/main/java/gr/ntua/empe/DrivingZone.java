package gr.ntua.empe;

import ch.hsr.geohash.BoundingBox;
import ch.hsr.geohash.WGS84Point;


public class DrivingZone extends BoundingBox {

    private String name;
    private Double speed;
    private Double vout;

    public DrivingZone(String name, Double speed, Double vout, double y1, double y2, double x1, double x2) {
        super(y1, y2, x1, x2);
        this.name = name;
        this.speed = speed;
        this.vout = vout;
    }

    public DrivingZone(String name, Double speed, Double vout, WGS84Point p1, WGS84Point p2) {
        super(p1, p2);
        this.name = name;
        this.speed = speed;
        this.vout = vout;
    }

    public String getName() {
        return name;
    }

    public Double getSpeed() {
        return speed;
    }

    public Double getVout() {
        return vout;
    }
}
